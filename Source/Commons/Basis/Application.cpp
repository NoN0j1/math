#include <DxLib.h>
#include "SceneController.h"
#include "Application.h"

namespace
{
	/// <summary>
	///	画面の横サイズ
	/// </summary>
	constexpr int default_window_width = 640;

	/// <summary>
	/// 画面の縦サイズ
	/// </summary>
	constexpr int default_window_height = 480;
}

Application& Application::Instance(void)
{
	static Application _instance;
	return _instance;
}

bool Application::Initialize(void)
{
	auto [wsizeX, wsizeY] = _config->GetWindowSize();
	DxLib::SetGraphMode(static_cast<int>(wsizeX), static_cast<int>(wsizeY), 32);
	DxLib::SetAlwaysRunFlag(true);
	DxLib::ChangeWindowMode(true);
	DxLib::SetWindowText(L"MathCollection");
	DxLib::SetWaitVSyncFlag(true);
	if (DxLib_Init() == -1)
	{
		return false;
	}
	_sceneController.reset(new SceneController());
	_input.reset(new Input());
	return true;
}

void Application::Run(void)
{
	while (DxLib::ProcessMessage() == 0)
	{
		_input->Update();
		ClsDrawScreen();
		_sceneController->Update(*_input);
		_sceneController->Draw();
		_sceneController->PostDrawUpdate();
		DxLib::ScreenFlip();
	}
}

void Application::Tarminate(void)
{
	DxLib::DxLib_End();
}

const Application::Config& Application::GetConfig(void) const
{
	return *_config;
}

Application::Application()
{
	_config = std::make_unique<Config>();
}

Application::Config::Config()
{
	_windowSize = std::make_tuple(default_window_width, default_window_height);
}

const std::tuple<int,int> Application::Config::GetWindowSize(void) const
{
	return _windowSize;
}