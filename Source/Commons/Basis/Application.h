#pragma once
#include <memory>
#include <tuple>
#include "Geometry.h"
#include "Input.h"

class SceneController;

/// <summary>
/// アプリケーション本体クラス
/// </summary>
class Application
{
public:
	~Application() = default;

	/// <summary>
	/// シングルトンの実態を返す
	/// </summary>
	/// <returns>Applicationの実体</returns>
	static Application& Instance(void);

	/// <summary>
	/// アプリケーションの初期化
	/// </summary>
	/// <returns>true = 初期化成功 false = 初期化失敗</returns>
	bool Initialize(void);

	/// <summary>
	/// アプリケーションを実行する
	/// </summary>
	void Run(void);

	/// <summary>
	/// 終了処理
	/// </summary>
	void Tarminate(void);

	/// <summary>
	/// アプリの設定項目クラス
	/// </summary>
	class Config
	{
	public:
		Config();
		~Config() = default;
		/// <summary>
		/// ゲーム画面サイズを取得する
		/// </summary>
		const std::tuple<int,int> GetWindowSize(void)const;
	private:
		/// <summary>
		/// 画面サイズ
		/// </summary>
		std::tuple<int, int> _windowSize;
	};

	/// <summary>
	/// 設定情報を取得する
	/// </summary>
	/// <returns>アプリ設定</returns>
	const Config& GetConfig(void)const;

private:
	///シングルトン処理------------------------------
	Application();
	Application(const Application&) = delete;
	void operator=(const Application&) = delete;
	///-----------------------------------------------
	
	/// <summary>
	/// アプリ設定管理クラスの実態保持
	/// </summary>
	std::unique_ptr<Config> _config;

	/// <summary>
	/// シーン管理クラスの実態保持
	/// </summary>
	std::unique_ptr<SceneController> _sceneController;
	
	/// <summary>
	/// 入力クラスの実態保持
	/// </summary>
	std::unique_ptr<Input> _input;
};