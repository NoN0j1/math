#include <DxLib.h>
#include "Application.h"

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Application& app = Application::Instance();
	if (app.Initialize() == false)
	{
		return -1;
	}
	app.Run();
	app.Tarminate();
	return 0;
}