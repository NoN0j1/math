#include <math.h>
#include <algorithm>
#include <cmath>
#include "DxLib.h"
#include "Geometry.h"

const double Vector2::Length(void)const
{
	return hypot(x, y);
}

const void Vector2::Normalize(void)
{
	auto len = Length();
	x /= len;
	y /= len;
}

const Vector2 Vector2::GetNormalized(void)const
{
	auto len = Length();
	return Vector2(x / len, y / len);
}

const double Vector3::Length(void)const
{
	return sqrt(x * x + y * y + z * z);
}

const void Vector3::Normalize(void)
{
	auto len = Length();
	x /= len;
	y /= len;
	z /= len;
}

const Vector3 Vector3::GetNormalized(void)const
{
	auto len = Length();
	if (len == 0)
	{
		return Vector3();
	}
	return Vector3(x / len, y / len, z / len);
}

const Vector3 Vector3::Reflect(const Vector3& normal)const
{
	return *this - normal * (2 * (Dot(this->GetNormalized(), normal)));
}


const Vector3 Position::ToVector3(void)const
{
	return Vector3(x, y, z);
}


const float Rect::Left(void)const
{
    return centerPosition.x - horizontalSide.x;
}

const float Rect::Right(void)const
{
    return centerPosition.x + horizontalSide.y;
}

const float Rect::Top(void)const
{
    return centerPosition.y - verticalSide.x;
}

const float Rect::Bottom(void)const
{
    return centerPosition.y + verticalSide.y;
}

bool RayTraceSphere::IsHitRay(const Line3D& ray, const Position& eye)
{
	//球の中心までのベクトル
	auto sphereVector = (sphere.position - eye).ToVector3();
	//射影長
	auto shadow = Dot(ray.vector, sphereVector);
	//球の中心から視線ベクトルに垂線を下した座標
	auto touchPos = ray.vector * shadow;
	//球の中心と垂線を下した座標の距離
	auto length = (sphereVector - touchPos).Length();

	bool isHit = length <= sphere.radius;
	if (isHit)
	{
		auto back = sqrt((static_cast<double>(sphere.radius) * static_cast<double>(sphere.radius)) - (length * length));
		auto surfaceLen = shadow - back;
		auto hit = eye + ray.vector * surfaceLen;
		surfacePos = Position(hit.x, hit.y, hit.z);
		normal = (surfacePos - sphere.position).ToVector3();
		normal.Normalize();
	}
	return isHit;
}

double Dot(const Vector2& va, const Vector2& vb)
{
	return va.x * vb.x + va.y * vb.y;
}

double Dot(const Vector3& va, const Vector3& vb)
{
	return va.x * vb.x + va.y * vb.y + va.z * vb.z;
}

Vector3 Cross(const Vector3& va, const Vector3& vb)
{
	return Vector3(va.z * vb.y - va.y * vb.z, va.z * vb.x - va.x * vb.z, va.x * vb.y - va.y * vb.x);
}

const Vector3 Line3D::GetLineVector(void) const
{
	return vector.GetNormalized();
}

Color::Color(int inR, int inG, int inB)
{
	r = std::clamp(std::clamp(inR, 0, 255) / 255.0f, 0.0f, 1.0f);
	g = std::clamp(std::clamp(inG, 0, 255) / 255.0f, 0.0f, 1.0f);
	b = std::clamp(std::clamp(inB, 0, 255) / 255.0f, 0.0f, 1.0f);
}

Color::Color(float inR, float inG, float inB)
{
	r = inR;
	g = inG;
	b = inB;
	/*r = std::clamp(inR, 0.0f, 1.0f);
	g = std::clamp(inG, 0.0f, 1.0f);
	b = std::clamp(inB, 0.0f, 1.0f);*/
}

int Color::GetDX_Color(void)
{
	return DxLib::GetColor(static_cast<int>(r * 255.0f), static_cast<int>(g * 255.0f), static_cast<int>(b * 255.0f));
}

void Color::saturate(void)
{
	r = std::clamp(r, 0.0f, 1.0f);
	g = std::clamp(g, 0.0f, 1.0f);
	b = std::clamp(b, 0.0f, 1.0f);
}

void Color::operator+=(const Color& val)
{
	r = std::clamp(r + val.r, 0.0f, 1.0f);
	g = std::clamp(g + val.g, 0.0f, 1.0f);
	b = std::clamp(b + val.b, 0.0f, 1.0f);
}

void Color::operator*=(const Color& val)
{
	r = std::clamp(r * val.r, 0.0f, 1.0f);
	g = std::clamp(g * val.g, 0.0f, 1.0f);
	b = std::clamp(b * val.b, 0.0f, 1.0f);
}

const Color Material::DiffuseColor(void)const
{
	return Lerp(albedo, Color(0.0f, 0.0f, 0.0f), metallic);
}

const Color Material::SpecularColor(void)const
{
	return Lerp(Color(0.04f, 0.04f, 0.04f), albedo, metallic);
}


Vector3 operator+(const Vector3& vec1, const Vector3& vec2)
{
	return Vector3(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
}

Vector3 operator-(const Vector3& vec1, const Vector3& vec2)
{
	return Vector3(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
}

Vector3 operator*(const Vector3& vec, const double& scale)
{
	return Vector3(vec.x * scale, vec.y * scale, vec.z * scale);
}

Vector3 operator+(const Position& pos, const Vector3& vec)
{
	return Vector3(pos.x + vec.x, pos.y + vec.y, pos.z + vec.z);
}

Vector3 operator-(const Position& pos, const Vector3& vec)
{
	return Vector3(pos.x - vec.x, pos.y - vec.y, pos.z - vec.z);
}

Position operator-(const Position& pos1, const Position& pos2)
{
	return Position(
		static_cast<double>(pos1.x) - static_cast<double>(pos2.x),
		static_cast<double>(pos1.y) - static_cast<double>(pos2.y),
		static_cast<double>(pos1.z) - static_cast<double>(pos2.z)
	);
}


Color operator+(const Color& color1, const Color& color2)
{
	return Color(color1.r + color2.r, color1.g + color2.g, color1.b + color2.b);
}

Color operator-(const Color& color1, const Color& color2)
{
	return Color(color1.r - color2.r, color1.g - color2.g, color1.b - color2.b);
}

Color operator*(const Color& color1, const Color& color2)
{
	return Color(color1.r * color2.r, color1.g * color2.g, color1.b * color2.b);
}

Color operator*(const Color& color, const float& scale)
{
	return Color(color.r * scale , color.g * scale, color.b * scale);
}

Color operator/(const Color& color, const float& scale)
{
	return Color(color.r / scale, color.g / scale, color.b / scale);
}

Color operator-(const float& base, const Color& color)
{
	return Color(base - color.r, base - color.g, base - color.b);
}

Color Lerp(const Color& a, const Color& b, float alpha)
{
	return a * (1 - alpha) + b * alpha;
}

std::tuple<bool, Position> RayTracePlane::IsHitRay(const Line3D& ray, const Position& eye)const
{
	bool isHit = false;
	if (eye.y == plane.offsetY)
	{
		isHit = false;
	}
	else if (eye.y > plane.offsetY)
	{
		isHit = Dot(plane.normal, -ray.vector) > 0;
	}
	else
	{
		isHit = Dot(plane.normal, -ray.vector) < 0;
	}
	
	Position hitPos;
	if (isHit)
	{
		auto eyeToPlaneLength = Dot(eye.ToVector3(), plane.normal) - plane.offsetY;
		auto rayPercentageToPlane = Dot(-ray.vector, plane.normal);
		auto t = eyeToPlaneLength / rayPercentageToPlane;
		auto hit = eye + (ray.vector * t);
		hitPos = Position(hit.x, hit.y, hit.z);
	}

	return { isHit, hitPos };
}

Color RayTracePlane::GetCheckerColorFromPosition(const Position& hitPos)const
{
	if (hitPos.x == 0 && hitPos.y == 0 && hitPos.z == 0)
	{
		return Color();
	}

	int x = static_cast<int>(hitPos.x);
	if (x >= 0)
	{
		x += 100;
	}
	int z = static_cast<int>(hitPos.z);

	if ((x / 100 + z / 100) % 2 == 0)
	{
		return Color(1.0f,1.0f, 1.0f);
	}
	else
	{
		return Color(0.2f, 0.2f, 0.2f);
	}
}

Obj::Obj()
{
}
