#pragma once
#include <tuple>

/// <summary>
/// 2Dベクトル
/// </summary>
struct Vector2
{
	Vector2() :x(0), y(0) {}
	Vector2(double inX, double inY) :x(inX), y(inY) {}
	double x;
	double y;

	inline void operator+=(const Vector2& val)
	{
		x += val.x;
		y += val.y;
	}
	inline void operator-=(const Vector2& val)
	{
		x -= val.x;
		y -= val.y;
	}
	inline void operator*=(const Vector2& val)
	{
		x *= val.x;
		y *= val.y;
	}
	inline void operator*=(const double scale)
	{
		x *= scale;
		y *= scale;
	}
	inline void operator/=(const double scale)
	{
		x /= scale;
		y /= scale;
	}
	inline Vector2 operator-(void)
	{
		return Vector2(-x, -y);
	}

	/// <summary>
	/// ベクトルの長さを返す
	/// </summary>
	/// <returns>ベクトルの長さ</returns>
	const double Length(void)const;

	/// <summary>
	/// 現在のベクトルを正規化する
	/// </summary>
	const void Normalize(void);

	/// <summary>
	/// 正規化ベクトルを取得する
	/// </summary>
	/// <returns>正規化済みベクトル</returns>
	const Vector2 GetNormalized(void)const;
};

/// <summary>
/// 3Dベクトル
/// </summary>
struct Vector3
{
	Vector3() :x(0), y(0), z(0) {}
	Vector3(double inX, double inY, double inZ) :x(inX), y(inY), z(inZ) {}

	double x;
	double y;
	double z;

	inline void operator+=(const Vector3& val)
	{
		x += val.x;
		y += val.y;
		z += val.z;
	}
	inline void operator-=(const Vector3& val)
	{
		x -= val.x;
		y -= val.y;
		z -= val.z;
	}
	inline void operator*=(const Vector3& val)
	{
		x *= val.x;
		y *= val.y;
		z *= val.z;
	}
	inline void operator*=(const double scale)
	{
		x *= scale;
		y *= scale;
		z *= scale;
	}
	inline void operator/=(const double scale)
	{
		x /= scale;
		y /= scale;
		z /= scale;
	}
	inline Vector3 operator-()const
	{
		return Vector3(-x, -y, -z);
	}

	/// <summary>
	/// ベクトルの長さを返す
	/// </summary>
	/// <returns>ベクトルの長さ</returns>
	const double Length(void)const;

	/// <summary>
	/// 現在のベクトルを正規化する
	/// </summary>
	const void Normalize(void);

	/// <summary>
	/// 正規化ベクトルを取得する
	/// </summary>
	/// <returns>正規化済みベクトル</returns>
	const Vector3 GetNormalized(void)const;

	/// <summary>
	/// 反射ベクトルを取得する
	/// </summary>
	/// <param name="normal">反射面の法線</param>
	/// <returns>反射後ベクトル</returns>
	const Vector3 Reflect(const Vector3& normal)const;
};

/// <summary>
/// 座標に使用する構造体
/// </summary>
struct Position
{
	Position(float inX = 0, float inY = 0, float inZ = 0) :x(inX), y(inY), z(inZ) {}
	float x;
	float y;
	float z;

	/// <summary>
	/// Vector3型に変換されたものを取得する
	/// </summary>
	/// <returns>Vector3型に入れた値</returns>
	const Vector3 ToVector3(void)const;

	inline void operator+=(const Position& val)
	{
		x += val.x;
		y += val.y;
		z += val.z;
	}
	inline void operator-=(const Position& val)
	{
		x -= val.x;
		y -= val.y;
		z -= val.z;
	}
	inline void operator+=(const Vector2& val)
	{
		x += static_cast<float>(val.x);
		y += static_cast<float>(val.y);
	}
	inline void operator-=(const Vector2& val)
	{
		x -= static_cast<float>(val.x);
		y -= static_cast<float>(val.y);
	}
	inline void operator+=(const Vector3& val)
	{
		x += static_cast<float>(val.x);
		y += static_cast<float>(val.y);
		z += static_cast<float>(val.z);
	}
	inline void operator-=(const Vector3& val)
	{
		x -= static_cast<float>(val.x);
		y -= static_cast<float>(val.y);
		z -= static_cast<float>(val.z);
	}
};

struct Line2D
{
	Line2D() {}
	Position startPosition;
	Vector2 vector;
};

struct Line3D
{
	Line3D(const Position& startPos, const Vector3& vec) : startPosition(startPos), vector(vec) { vector.Normalize(); }
	Position startPosition;
	Vector3 vector;
	
	/// <summary>
	/// 方向ベクトルを取得する(正規化済み)
	/// </summary>
	/// <returns>正規化済みベクトル</returns>
	const Vector3 GetLineVector(void)const;
};


/// <summary>
/// 矩形クラス
/// </summary>
struct Rect
{
	Rect() :centerPosition(Position()), horizontalSide(Position()), verticalSide(Position()){}

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name="c">基準座標</param>
		/// <param name="h">左右の辺座標</param>
	/// <param name="v">上下の辺座標</param>
	Rect(const Position& c, const Position& h, const Position& v) :centerPosition(c), horizontalSide(h), verticalSide(v) {};

	/// <summary>
	/// 中央座標
	/// </summary>
	Position centerPosition;

	/// <summary>
	/// 上下方向の辺座標
	/// </summary>
	Position verticalSide;

	/// <summary>
	/// 左右方向の辺座標
	/// </summary>
	Position horizontalSide;

	/// <summary>
	/// 中心座標から左の長さ分引いた座標を返す
	/// </summary>
	/// <returns>X座標</returns>
	const float Left(void)const;

	/// <summary>
	/// 中心座標から右の長さ分足した座標を返す
	/// </summary>
	/// <returns>X座標</returns>
	const float Right(void)const;

	/// <summary>
	/// 中心座標から上の長さ分引いた座標を返す
	/// </summary>
	/// <returns>Y座標</returns>
	const float Top(void)const;

	/// <summary>
	/// 中心座標から下の長さ分足した座標を返す
	/// </summary>
	/// <returns>Y座標</returns>
	const float Bottom(void)const;
};

struct Obj
{
	Obj();
};

/// <summary>
/// 球
/// </summary>
struct Sphere : Obj
{
	Sphere(float inX, float inY, float inZ, float rad) :position(inX, inY, inZ), radius(rad) {}
	Sphere(const Position& pos, float rad) :position(pos), radius(rad){}

	/// <summary>
	/// 中心座標
	/// </summary>
	Position position;

	/// <summary>
	/// 半径
	/// </summary>
	float radius;
};

/// <summary>
/// 床
/// </summary>
struct Plane : Obj
{
	Plane(const Vector3& inN, float inOffset) : normal(inN), offsetY(inOffset) 
	{
		normal.Normalize();
	};

	Vector3 normal;
	float offsetY;
};


/// <summary>
/// 2Dベクトルの内積の計算結果を返す
/// </summary>
/// <param name="va">ベクトルA</param>
/// <param name="vb">ベクトルB</param>
/// <returns>→A・→B</returns>
double Dot(const Vector2& va, const Vector2& vb);

/// <summary>
/// 3Dベクトルの内積の計算結果を返す
/// </summary>
/// <param name="va">ベクトルA</param>
/// <param name="vb">ベクトルB</param>
/// <returns>→A・→B</returns>
double Dot(const Vector3& va, const Vector3& vb);

/// <summary>
/// 3Dベクトルの外積の計算結果を返す
/// </summary>
/// <param name="va">ベクトルA</param>
/// <param name="vb">ベクトルB</param>
/// <returns>→A × →B</returns>
Vector3 Cross(const Vector3& va, const Vector3& vb);

/// <summary>
/// 色
/// </summary>
struct Color
{
	Color(int inR, int inG, int inB);
	Color(float inR = 1.0f, float inG = 1.0f, float inB = 1.0f);
	float r;
	float g;
	float b;

	/// <summary>
	/// DxLibで描画する際のintデータを取得する
	/// </summary>
	/// <returns>カラーデータ</returns>
	int GetDX_Color(void);

	/// <summary>
	/// 色を0.0〜1.0の範囲に収める
	/// </summary>
	void saturate(void);
	void operator+=(const Color& val);
	void operator*=(const Color& val);
};

/// <summary>
/// 材質
/// </summary>
struct Material
{
	Material() :albedo(Color(1.0f, 1.0f, 1.0f)), metallic(0.5f), roughness(0.3f), opacity(1.0f){}
	
	/// <param name="c">反射色</param>
	/// <param name="m">metalic</param>
	/// <param name="r">roughness</param>
	/// <param name="a">透明度</param>
	/// <returns></returns>
	Material(Color c, float m = 0.5f, float r = 0.3f, float a = 1.0f) : albedo(c), metallic(m), roughness(r), opacity(a) {}

	/// <summary>
	/// 鏡面反射率
	/// </summary>
	float metallic;

	/// <summary>
	/// 表面の粗さ
	/// </summary>
	float roughness;

	/// <summary>
	/// 反射能(要するに、何色を反射させるか→何色に見えるか)
	/// </summary>
	Color albedo;

	/// <summary>
	/// 透明率
	/// </summary>
	float opacity;

	/// <summary>
	/// PBRdiffuseを取得する
	/// </summary>
	/// <returns>Diffuse</returns>
	const Color DiffuseColor(void)const;

	/// <summary>
	/// PRBspecularを取得する
	/// </summary>
	/// <returns>Specular</returns>
	const Color SpecularColor(void)const;
};

/// <summary>
/// レイトレーシングオブジェクト
/// </summary>
struct RayTraceObject
{
	RayTraceObject(const Material& mat) : material(mat){}
	Material material;
};

/// <summary>
/// レイトレーシングに使用する球
/// </summary>
struct RayTraceSphere : RayTraceObject
{
	RayTraceSphere(Material mat, Sphere inSphere) :RayTraceObject(mat), sphere(inSphere) {}
	Sphere sphere;
	Vector3 normal = {};
	Position surfacePos = {};

	/// <summary>
	/// 球にベクトルが当たっているかを取得する
	/// </summary>
	/// <param name="ray">正規化済みベクトルと視点座標</param>
	/// <returns>bool = 当たっているかどうか</returns>
	bool IsHitRay(const Line3D& ray, const Position& eye);
};

struct RayTracePlane : RayTraceObject
{
	RayTracePlane(Material mat, Plane inPlane) : RayTraceObject(mat), plane(inPlane) {}
	Plane plane;

	/// <summary>
	/// 直線(ベクトル)が交差しているかを取得する
	/// </summary>
	/// <param name="ray">直線</param>
	/// <returns>bool = 交差しているかどうか、 Position = 交差していた場合の座標</returns>
	std::tuple<bool, Position> IsHitRay(const Line3D& ray, const Position& eye)const;

	/// <summary>
	/// 交点の色を返す
	/// </summary>
	/// <param name="eye">交点座標</param>
	/// <returns>交点の床の色</returns>
	Color GetCheckerColorFromPosition(const Position& hitPos)const;
};

Vector3 operator+(const Vector3& vec1, const Vector3& vec2);
Vector3 operator-(const Vector3& vec1, const Vector3& vec2);
Vector3 operator*(const Vector3& vec, const double& scale);
Vector3 operator+(const Position& pos, const Vector3& vec);
Vector3 operator-(const Position& pos, const Vector3& vec);
Position operator-(const Position& pos1, const Position& pos2);
Color operator+(const Color& color1, const Color& color2);
Color operator-(const Color& color1, const Color& color2);
Color operator*(const Color& color1, const Color& color2);
Color operator*(const Color& color, const float& scale);
Color operator/(const Color& color, const float& scale);
Color operator-(const float& base, const Color& color);
Color Lerp(const Color& a, const Color& b, float alpha);