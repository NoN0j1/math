#include "Input.h"

Input::Input()
{
    _mouse.reset(new MouseInput());
    _keyboard.reset(new KeyboardInput());
}

void Input::Update(void)
{
    _mouse->Update();
    _keyboard->Update();
}

const MouseInput& Input::GetMouse() const
{
    return *_mouse.get();
}

const KeyboardInput& Input::GetKeyboard() const
{
    return *_keyboard.get();
}
