#include <DxLib.h>
#include "KeyboardInput.h"

KeyboardInput::KeyboardInput()
{
	_keyState = {};
	_oldKeyState = {};
}


const bool KeyboardInput::IsPush(int dx_type) const
{
	return (_keyState[dx_type] && !_oldKeyState[dx_type]);
}

const bool KeyboardInput::IsHold(int dx_type) const
{
	return (_keyState[dx_type] && _oldKeyState[dx_type]);
}

const bool KeyboardInput::IsRelease(int dx_type) const
{
	return (!_keyState[dx_type] && _oldKeyState[dx_type]);
}

void KeyboardInput::Update(void)
{
	std::copy(_keyState.begin(), _keyState.end(), _oldKeyState.data());
	DxLib::GetHitKeyStateAll(_keyState.data());
}
