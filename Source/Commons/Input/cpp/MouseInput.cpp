#include <DxLib.h>
#include "MouseInput.h"

MouseInput::MouseInput()
{
    _inputData = 0;
    _inputDataOld = 0;
    _position = Position();
    _positionOld = _position;
}

const bool MouseInput::IsClick(EMouseInputType type)const
{
    return IsPush(type) && !IsPushOld(type);
}

const bool MouseInput::IsHold(EMouseInputType type)const
{
    return IsPush(type)&& IsPushOld(type);
}

const bool MouseInput::IsRelease(EMouseInputType type)const
{
    return !IsPush(type) && IsPushOld(type);
}

const Position& MouseInput::GetPosition(void)const
{
    return _position;
}

const Vector3 MouseInput::GetMoveVector(void) const
{
    return (_position - _positionOld).ToVector3().GetNormalized();
}


void MouseInput::Update(void)
{
    // ﾏｳｽの座標取得
    _positionOld = _position;
    int tempX, tempY = 0;
    DxLib::GetMousePoint(&tempX, &tempY);
    _position.x = static_cast<float>(tempX);
    _position.y = static_cast<float>(tempY);
    // ﾏｳｽの入力情報取得
    _inputDataOld = _inputData;
    _inputData = static_cast<char>(GetMouseInput());
}

const bool MouseInput::IsPush(EMouseInputType type)const
{
    int bit = (_inputData & (1 << static_cast<int>(type)));
    return (bit != 0);
}

const bool MouseInput::IsPushOld(EMouseInputType type)const
{
    int bit = (_inputDataOld & (1 << static_cast<int>(type)));
    return (bit != 0);
}
