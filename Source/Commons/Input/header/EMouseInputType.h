#pragma once

/// <summary>
/// マウス入力種別
/// </summary>
enum class EMouseInputType
{
	Left,
	Right,
	Wheel,
	MAX
};
