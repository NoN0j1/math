#pragma once
#include <memory>
#include "MouseInput.h"
#include "KeyboardInput.h"

/// <summary>
/// 入力情報全般を管理するクラス
/// </summary>
class Input
{
	friend class Application;
public:
	~Input() = default;

	/// <summary>
	/// 入力情報を更新する
	/// </summary>
	void Update(void);

	/// <summary>
	/// マウスの入力情報を取得する
	/// </summary>
	/// <returns>マウス入力情報</returns>
	const MouseInput& GetMouse(void)const;

	/// <summary>
	/// キーボードの入力情報を取得する
	/// </summary>
	/// <returns>キーボード入力情報</returns>
	const KeyboardInput& GetKeyboard(void)const;
private:
	Input();
	/// <summary>
	/// マウス入力管理クラスの実態保持
	/// </summary>
	std::unique_ptr<MouseInput> _mouse;

	/// <summary>
	/// キーボード入力管理クラスの実態保持
	/// </summary>
	std::unique_ptr<KeyboardInput> _keyboard;
};

