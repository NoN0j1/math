#pragma once
#include <array>

constexpr int keyborad_buffer_size = 256;

class KeyboardInput
{
	friend class Input;
public:
	~KeyboardInput() = default;

	/// <summary>
	/// ボタンを押した瞬間を取得する
	/// </summary>
	/// <param name="dx_type"></param>
	/// <returns>true = 押した瞬間である</returns>
	const bool IsPush(int dx_type)const;

	/// <summary>
	/// ボタンを押したままにしているかを取得する
	/// </summary>
	/// <returns>true = ホールドしている状態である</returns>
	const bool IsHold(int dx_type)const;

	/// <summary>
	/// ボタンを離した瞬間を取得する
	/// </summary>
	/// <returns>true = 離した瞬間である</returns>
	const bool IsRelease(int dx_type)const;
private:
	KeyboardInput();

	/// <summary>
	/// 毎フレームの入力更新
	/// </summary>
	void Update(void);

	std::array<char, keyborad_buffer_size> _keyState;
	std::array<char, keyborad_buffer_size> _oldKeyState;
};

