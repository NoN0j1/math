#pragma once
#include "Geometry.h"
#include "EMouseInputType.h"

/// <summary>
/// マウス入力情報を管理するクラス
/// </summary>
class MouseInput
{
	friend class Input;
public:
	~MouseInput() = default;

	/// <summary>
	/// ボタンを押した瞬間を取得する
	/// </summary>
	/// <returns>true = 押した瞬間である</returns>
	const bool IsClick(EMouseInputType type)const;

	/// <summary>
	/// ボタンを押したままにしているかを取得する
	/// </summary>
	/// <returns>true = ホールドしている状態である</returns>
	const bool IsHold(EMouseInputType type)const;

	/// <summary>
	/// ボタンを離した瞬間を取得する
	/// </summary>
	/// <returns>true = 離した瞬間である</returns>
	const bool IsRelease(EMouseInputType type)const;

	/// <summary>
	/// カーソル座標を取得する
	/// </summary>
	/// <returns>画面上の座標</returns>
	const Position& GetPosition(void)const;

	/// <summary>
	/// カーソルの移動ベクトルを取得する
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	const Vector3 GetMoveVector(void)const;

private:
	MouseInput();

	/// <summary>
	/// 毎フレームの入力更新
	/// </summary>
	void Update(void);

	/// <summary>
	/// 最終更新時にボタンが押されていたかを確認する
	/// </summary>
	/// <param name="type">ボタン種別</param>
	/// <returns>true = 押されている false = 押されていない</returns>
	const bool IsPush(EMouseInputType type)const;

	/// <summary>
	/// 更新前にボタンが押されていたかを確認する
	/// </summary>
	/// <param name="type">ボタン種別</param>
	/// <returns>true = 押されていた false = 押されていない</returns>
	const bool IsPushOld(EMouseInputType type)const;

	/// <summary>
	/// マウス座標
	/// </summary>
	Position _position;

	/// <summary>
	/// 更新前のマウス座標
	/// </summary>
	Position _positionOld;

	/// <summary>
	/// 現在のマウス入力情報
	/// </summary>
	char _inputData;
	
	/// <summary>
	/// 更新前のマウス入力情報
	/// </summary>
	char _inputDataOld;
};

