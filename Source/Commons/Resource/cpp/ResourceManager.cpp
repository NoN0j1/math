#include <sstream>
#include <cassert>
#include <DxLib.h>
#include "ResourceManager.h"

ResourceManager::ResourceManager()
{
}

ResourceManager& ResourceManager::Instance(void)
{
    static ResourceManager _instace;
    return _instace;
}

bool ResourceManager::RegisterImage(const char* name, const wchar_t* path)
{
    //重複チェック
    if (_textureResource.contains(name))
    {
        return false;
    }

    auto tex = LoadGraphResource(path);
    if (tex->Handle == -1)
    {
        return false;
    }
    _textureResource.emplace(name, tex);
    
    return true;
}

bool ResourceManager::RegisterDivImage(const char* name, const wchar_t* path, int divX, int divY, int total)
{
    //重複チェック
    if (_divTextureResource.contains(name))
    {
        return false;
    }

    //分割数0の場合は終了
    if (divX == 0 || divY == 0)
    {
        return false;
    }

    //分割数がX,Yともに1の場合は普通に読み込めばいいので通常読み込みを行なう
    if (divX == 1 && divY == 1)
    {
        return RegisterImage(name, path);
    }

    auto tex = LoadGraphResource(path);
    if (tex->Handle == -1)
    {
        return false;
    }

    std::vector<int> loadTemp(total);
    std::wstringstream pathWstring;
    pathWstring << L"Resource/Image/" << path;
    int divSizeX = tex->Width / divX;
    int divSizeY = tex->Height / divY;
    DxLib::LoadDivGraph(pathWstring.str().c_str(), total, divX, divY, divSizeX, divSizeY, loadTemp.data());
    
    std::vector<Texture*> addData;
    for (int& handle : loadTemp)
    {
        addData.push_back(new Texture(handle, divSizeX, divSizeY));
    }
    
    _divTextureResource.emplace(name, addData);
    return true;
}

const Texture& ResourceManager::GetTexture(const char* name)const
{
    if (!_textureResource.contains(name))
    {
        return *(new Texture());
    }
    return *_textureResource.at(name);
}

const Texture& ResourceManager::GetDivTexture(const char* name, int num)const
{
    if (!_divTextureResource.contains(name))
    {
        return *(new Texture());
    }

    auto& data = _divTextureResource.at(name);
    if (data.size() <= num)
    {
        return *(new Texture());
    }
    return *data.at(num);
}

bool ResourceManager::CreateDXFont(const wchar_t* fontName, const wchar_t* name, int size, int thick, int fontType)
{
    if (_dxfont.contains(name))
    {
        return false;
    }

    int handle = DxLib::CreateFontToHandle(fontName, size, thick, fontType);
    if (handle == -1)
    {
        return false;
    }
    _dxfont.emplace(name, handle);
    return true;
}

int ResourceManager::GetDXFont(const wchar_t* name)
{
    if (!_dxfont.contains(name))
    {
        return -1;
    }
    return _dxfont.at(name);
}

Texture* ResourceManager::LoadGraphResource(const wchar_t* path)
{
    std::wstringstream wss;
    wss << L"Resource/Image/" << path;
    int handle =  DxLib::LoadGraph(wss.str().c_str());
    if (handle == -1)
    {
        return new Texture();
    }
    int masterSizeX, masterSizeY = 0;
    DxLib::GetGraphTextureSize(handle, &masterSizeX, &masterSizeY);
    return new Texture(handle, masterSizeX, masterSizeY);
}
