#pragma once
#include <unordered_map>
#include <string>
#include "ResourceStructure.h"

/// <summary>
/// リソース管理クラス
/// </summary>
class ResourceManager
{
public:
	~ResourceManager() = default;

	/// <summary>
	/// シングルトンの実態を返す
	/// </summary>
	/// <returns>TextureManagerの実体</returns>
	static ResourceManager& Instance(void);

	/// <summary>
	/// 画像リソースをメモリ上に読み込む
	/// </summary>
	/// <param name = "name"> 呼び出す時の登録名</param>
	/// <param name = "path"> Resource/Image/以降のPath</param>
	/// <returns>true = 読み込み成功 false = 読み込み失敗 or 既に登録済みの登録名だった</returns>
	bool RegisterImage(const char* name, const wchar_t* path);

	/// <summary>
	/// 画像リソースを分割してメモリ上に読み込む
	/// </summary>
	/// <param name = "name"> 呼び出す時の登録名</param>
	/// <param name = "path"> Resource/Image/以降のPath</param>
	/// <param name = "divX"> 横の分割数</param>
	/// <param name = "divY"> 縦の分割数</param>
	/// <param name = "total"> 画像総数</param>
	/// <returns>true = 読み込み成功 false = 読み込み失敗 or 既に登録済みの登録名だった</returns>
	bool RegisterDivImage(const char* name, const wchar_t* path, int divX, int divY, int total);

	/// <summary>
	/// 画像リソースのハンドルを取得する
	/// </summary>
	/// <param name="name">登録した画像名</param>
	/// <returns>画像情報</returns>
	const Texture& GetTexture(const char* name)const;

	/// <summary>
	/// 分割読み込みした画像ハンドルを取得する
	/// </summary>
	/// <param name="name">登録名</param>
	/// <param name="num">画像番号</param>
	/// <returns>画像情報</returns>
	const Texture& GetDivTexture(const char* name, int num)const;

	/// <summary>
	/// DxLib用フォントデータを作成する
	/// </summary>
	/// <param name="fontName">フォント名</param>
	/// <param name="name">登録名</param>
	/// <param name="size">フォントサイズ</param>
	/// <param name="thick">太さ</param>
	/// <param name="fontType">DX_FONTTYPE</param>
	/// <returns>true = 成功, false = 作成に失敗</returns>
	bool CreateDXFont(const wchar_t* fontName, const wchar_t* name, int size, int thick, int fontType);

	/// <summary>
	/// 作成したDxLib用フォントデータを取得する
	/// </summary>
	/// <param name="name">登録フォント名</param>
	int GetDXFont(const wchar_t* name);

private:
	ResourceManager();
	ResourceManager(const ResourceManager&) = delete;
	void operator = (const ResourceManager&) = delete;

	/// <summary>
	/// 画像データの読み込み(共通)
	/// </summary>
	/// <returns>画像ハンドル (DxLibの仕様で-1の場合は読み込みに失敗)</returns>
	Texture* LoadGraphResource(const wchar_t* path);
	
	/// <summary>
	/// 画像リソース
	/// </summary>
	std::unordered_map<std::string, Texture*> _textureResource;

	/// <summary>
	/// 分割画像リソース
	/// </summary>
	std::unordered_map<std::string, std::vector<Texture*>> _divTextureResource;

	/// <summary>
	/// DxLib用作成フォントデータ
	/// </summary>
	std::unordered_map<std::wstring, int> _dxfont;
};
