#pragma once

/// <summary>
/// �摜���
/// </summary>
struct Texture
{
public:
	Texture() :Handle(-1), Width(0), Height(0) {};

	/// <param name="handle">�摜�n���h��</param>
	/// <param name="w">����</param>
	/// <param name="h">�c��</param>
	Texture(int handle, int w, int h) :Handle(handle), Width(w), Height(h) {};

	/// <summary>
	/// �摜�n���h��(DxLib)
	/// </summary>
	int Handle;

	/// <summary>
	/// �摜�̉���
	/// </summary>
	int Width;

	/// <summary>
	/// �摜�̏c��
	/// </summary>
	int Height;
};