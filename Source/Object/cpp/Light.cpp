#include <math.h>
#include <algorithm>
#include <DxLib.h>
#include "Light.h"

namespace
{
	constexpr float epsilon = 0.00005f;
}

DirectionalLight::DirectionalLight(const Vector3& dir, const Color& lightColor) :Light(lightColor)
{
	direction = dir;// .GetNormalized();
}

void RenderEquations(const IncidentLight& directLight, const Vector3& normal, const Vector3& ray, const Material& material, ReflectedLightColor& reflectColor)
{
	double dotNL = std::clamp(Dot(normal, directLight.direction), 0.0, 1.0);
	if (dotNL > 0.9)
	{
		int a = 0;
		a++;
	}
	Color colorStrength = directLight.color * static_cast<float>(dotNL);
	colorStrength = colorStrength * DX_PI_F;
	reflectColor.directDiffuse += colorStrength * DiffuseBRDF(material.DiffuseColor());
	reflectColor.directSpecular += colorStrength * SpecularBRDF(directLight, -ray, normal, material.SpecularColor(), material.roughness);
}

bool CheckLightRange(const float lightDistance, const float lightRange)
{
	bool ret = true;
	ret &= (0.0f < lightRange);
	ret &= (lightDistance < lightRange);
	return ret;
}

float LightIntensityFactor(const float lightDistance, const float lightRange, const float decay)
{
	if (decay <= 0.0f)
	{
		//�����Ȃ�
		return 1.0f;
	}
	return powf((-lightDistance / lightDistance + 1.0f), decay);
}

Color DiffuseBRDF(const Color& diffuse)
{
	return diffuse / DX_PI_F;
}

Color SpecularBRDF(const IncidentLight& inLight, const Vector3& ray, const Vector3& normal, const Color& specularColor, float roughness)
{
	float dotNL = static_cast<float>(std::clamp(Dot(normal, inLight.direction), 0.0, 1.0));
	float dotNV = static_cast<float>(std::clamp(Dot(normal, ray), 0.0, 1.0));
	Vector3 h = (ray + inLight.direction).GetNormalized();
	float dotNH = static_cast<float>(std::clamp(Dot(normal, h), 0.0, 1.0));
	float dotVH = static_cast<float>(std::clamp(Dot(ray, h), 0.0, 1.0));
	float dotLV = static_cast<float>(std::clamp(Dot(inLight.direction, ray), 0.0, 1.0));
	float a = roughness * roughness;

	float D = D_GGXDistribution(a, dotNH);
	float G = G_SmithJointGGX(a, dotNV, dotNL);
	Color F = F_FresnelReflect(specularColor, ray, h);
	return (F * (G * D)) / (4.0f * dotNL * dotNV + epsilon);
}

float D_GGXDistribution(float a, float dotNH)
{
	float a2 = a * a;
	float dotNH2 = dotNH * dotNH;
	float d = dotNH2 * (a2 - 1.0f) + 1.0f;
	return a2 / (DX_PI_F * d * d);
}

float G_SmithJointGGX(float a, float dotNV, float dotNL)
{
	float alphaRoughnessSqrt = a * a;
	float GGXV = dotNL * sqrtf(dotNV * dotNV * (1.0f - alphaRoughnessSqrt) + alphaRoughnessSqrt);
	float GGXL = dotNV * sqrtf(dotNV * dotNV * (1.0f - alphaRoughnessSqrt) + alphaRoughnessSqrt);

	float GGX = GGXV + GGXL;
	if (GGX > 0.0f)
	{
		return 0.5f / GGX;
	}

	return 0.0f;
}

Color F_FresnelReflect(const Color& specularColor, const Vector3& h, const Vector3& v)
{
	return (specularColor + (1.0f - specularColor) * static_cast<float>(pow(std::clamp(1.0f - Dot(v, h), 0.0, 1.0), 5.0)));
}

IncidentLight::IncidentLight(const Vector3& dir, const Color& c) : color(c)
{
	direction.x = -dir.x;
	direction.y = -dir.y;
	direction.z = -dir.z;
}