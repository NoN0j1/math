#pragma once
#include "Geometry.h"

/// <summary>
/// 入射光
/// </summary>
struct IncidentLight
{
	IncidentLight(const Vector3& dir, const Color& c);
	bool visible = true;
	Vector3 direction;
	Color color;
};

struct Light
{
	Light(Color c):lightColor(c) {}
	/// <summary>
	/// ライトの色
	/// </summary>
	Color lightColor;
};

struct DirectionalLight : Light
{
	DirectionalLight(const Vector3& dir, const Color& lightColor = Color(1.0f, 1.0f, 1.0f));

	/// <summary>
	/// 方向
	/// </summary>
	Vector3 direction;
};

struct PointLight : Light
{
	/// <summary>
	/// 光源の位置
	/// </summary>
	Vector3 position;

	/// <summary>
	/// 光の届く距離
	/// </summary>
	float range;

	/// <summary>
	/// 減衰率
	/// </summary>
	float decay;
};

struct SpotLight : Light
{
	SpotLight(const Position& pos, const Vector3& dir, float r, float decay, float coneSize, float widthDecay, const Color& lightColor = Color(1.0f, 1.0f, 1.0f)) 
		:position(pos), direction(dir), range(r),decay(decay),coneCos(coneSize), widthDecayCos(widthDecay), Light(lightColor) {}

	/// <summary>
	/// 光源の位置
	/// </summary>
	Position position;

	/// <summary>
	/// 光源方向
	/// </summary>
	Vector3 direction;

	/// <summary>
	/// 光の届く距離
	/// </summary>
	float range;

	/// <summary>
	/// 減衰率
	/// </summary>
	float decay;

	/// <summary>
	/// 光線の幅(円錐の大きさ)
	/// </summary>
	float coneCos;

	/// <summary>
	/// 照射光線の減衰幅
	/// </summary>
	float widthDecayCos;
};

struct ReflectedLightColor 
{
	ReflectedLightColor()
		: directDiffuse(Color(0.0f, 0.0f, 0.0f)), directSpecular(Color(0.0f, 0.0f, 0.0f)), 
		indirectDiffuse(Color(0.0f, 0.0f, 0.0f)), indirectSpecular(Color(0.0f, 0.0f, 0.0f)) {}

	/// <summary>
	/// 直接光のDiffuse;
	/// </summary>
	Color directDiffuse;

	/// <summary>
	/// 直接光のSpecular
	/// </summary>
	Color directSpecular;

	/// <summary>
	/// 間接光のDiffuse;
	/// </summary>
	Color indirectDiffuse;

	/// <summary>
	/// 間接光のSpecular
	/// </summary>
	Color indirectSpecular;
};

/// <summary>
/// 最終的な反射色を計算する
/// </summary>
/// <param name="directLight">直接入射光</param>
/// <param name="normal">物体の法線</param>
/// <param name="ray">視線ベクトル</param>
/// <param name="material">マテリアル情報</param>
/// <param name="reflectColor">反射色、この参照の値を更新する。</param>
void RenderEquations(const IncidentLight& directLight, const Vector3& normal, const Vector3& ray, const Material& material, ReflectedLightColor& reflectColor);

/// <summary>
/// ライトが物体に届いているかどうかを取得する
/// </summary>
/// <param name="lightDistance">ライトと物体の距離</param>
/// <param name="lightRange">ライトの照射距離</param>
/// <returns>true = 届いている, false = 届いていない</returns>
bool CheckLightRange(const float lightDistance, const float lightRange);

/// <summary>
/// ライトにより照らされる強度を取得する
/// </summary>
/// <param name="lightDistance">ライトと物体の距離</param>
/// <param name="lightRange">ライトの照射距離</param>
/// <param name="decay">スクリーン上の高さ</param>
/// <returns>照射強度</returns>
float LightIntensityFactor(const float lightDistance, const float lightRange, const float decay);

/// <summary>
/// ランベルトの余原則で明るさを求める
/// </summary>
/// <param name="diffuse">マテリアルのDiffuseColor</param>
/// <returns>明るさを加味した場合の色</returns>
Color DiffuseBRDF(const Color& diffuse);


/// <summary>
/// クック・トランスモデルで反射色を求める
/// </summary>
/// <param name="inLight">入射光</param>
/// <param name="ray">視線ベクトル</param>
/// <param name="normal">反射する物体の法線</param>
/// <param name="specularColor">マテリアルの反射色</param>
/// <param name="roughness">反射の粗さ</param>
/// <returns>反射光部分の色</returns>
Color SpecularBRDF(const IncidentLight& inLight, const Vector3& ray, const Vector3& normal, const Color& specularColor, float roughness);

/// <summary>
/// 微小面法線分布関数関数としてGGX分布で取得する
/// </summary>
float D_GGXDistribution(float a, float dotNH);

/// <summary>
/// 幾何減衰
/// </summary>
/// <returns></returns>
float G_SmithJointGGX(float a, float dotNV, float dotNL);

/// <summary>
/// フレネル反射率をSchlickの式で求める
/// </summary>
Color F_FresnelReflect(const Color& specularColor, const Vector3& h, const Vector3& v);