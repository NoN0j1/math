#include <cassert>
#include <functional>
#include <algorithm>
#include <DxLib.h>
#include "Application.h"
#include "AppMainScene.h"
#include "ResourceManager.h"

namespace
{
	Position ScreenPos = {};
	float fixX = 0;
	float fixY = 0;
}

AppMainScene::AppMainScene(SceneController& c) : SceneBase(c, L"MainScene")
{
	if (!Initialize())
	{
		assert(0);
	}
}

bool AppMainScene::Initialize(void)
{
	auto& [screen_width, screen_height] = Application::Instance().GetConfig().GetWindowSize();
	fixX = screen_width / 2;
	fixY = screen_height / 2;
	ScreenPos = Position(static_cast<float>(fixX), static_cast<float>(fixY), 0);
	DxLib::SetUseZBuffer3D(true);
	DxLib::SetWriteZBuffer3D(true);

	bool isSuccess = true;
	return isSuccess;
}

void AppMainScene::Update(const Input& input)
{
	if (_updater == nullptr)
	{
		return;
	}
	(this->*_updater)(input);
}

void AppMainScene::Draw(void)
{
	DxLib::VECTOR vec1;
	DxLib::VECTOR vec2;
	vec1.x = ScreenPos.x;
	vec1.y = ScreenPos.y;
	vec1.z = 1000;
	
	vec2.x = -0.5;
	vec2.z = -0.5;
	
	DxLib::DrawSphere3D(VGet(ScreenPos.x, ScreenPos.y, 0), 100, 100, 0xffffff, 0xffffff, true);
	if (_drawer == nullptr)
	{
		return;
	}
	(this->*_drawer)();
}
