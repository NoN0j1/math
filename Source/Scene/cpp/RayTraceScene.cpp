#include <cassert>
#include <algorithm>
#include <DxLib.h>
#include "RayTraceScene.h"
#include "ResourceManager.h"

namespace
{
	enum State
	{
		Phong,
		PBR
	};
	Position ScreenPos = {};
	int stateNum = Phong;
	constexpr int reflect_max = 3;
	constexpr int draw_explanation_offset_x = 5;
	constexpr int draw_explanation_offset_y = 100;
	constexpr int draw_stateguide_offset_y = 75;
	constexpr int cursor_speed = 10;
	constexpr double light_speed = 0.05f;
	std::vector< std::pair< std::pair<std::wstring, float*>, std::wstring> >  DrawExplanationParameter;
	std::vector< std::pair< std::pair<std::wstring, double*>, std::wstring> >  DrawParameterValue;
	std::vector<std::wstring>  DrawGuidText;
}

RayTraceScene::RayTraceScene(SceneController& c) : SceneBase(c, L"RayTracing")
{
	if (!Initialize())
	{
		assert(0);
	}
}

bool RayTraceScene::Initialize(void)
{
	//関数ポインタの初期登録
	stateNum = Phong;
	_lightingSphereFunction = &RayTraceScene::Lighting;
	_lightingPlaneFunction = &RayTraceScene::LightingPlane;

	auto& [screen_width, screen_height] = Application::Instance().GetConfig().GetWindowSize();
	_screen = DxLib::MakeScreen(screen_width, screen_height, true);
	_update = true;
	//視点
	_eye = { 0, 0, -300 };
	
	//球
	_spheres.push_back(std::make_shared<RayTraceSphere>(Material(Color(1.0f, 1.0f, 1.0f),0.5f, 0.5f), Sphere(Position(0, 0, 100), 100)));

	//Zソートをする
	std::sort(_spheres.begin(), _spheres.end(),
		[](const std::shared_ptr<RayTraceSphere>& s1, const std::shared_ptr<RayTraceSphere>& s2) -> 
		float{ return  s1->sphere.position.z < s2->sphere.position.z; });

	//床

	_plane = std::make_shared<RayTracePlane>(Material(Color(1.0f), 0.5f, 0.5f), Plane(Vector3(0, 1, 0), -100));
	//ライト
	_directionalLight = std::make_shared<DirectionalLight>(Vector3(1, -1, 1));
	_spotLight = std::make_shared<SpotLight>(Position(0, 150, 100), Vector3(0, -1, 0),150, 0.05f, 0.5f, 0.01f);

	//描画するパラメータを追加
	DrawGuidText.push_back(L"L_CTRL : PhongとPBRの切替");
	DrawExplanationParameter.push_back(std::make_pair( std::make_pair(L"EyeX : %.2f", &_eye.x), L"右クリック押しながらマウスで移動"));
	DrawExplanationParameter.push_back(std::make_pair( std::make_pair(L"EyeY : %.2f", &_eye.y), L"右クリック押しながらマウスで移動"));
	DrawExplanationParameter.push_back(std::make_pair( std::make_pair(L"Metalic : %.2f", &_spheres[0]->material.metallic), L"[1]で増加,[2]で減少"));
	DrawExplanationParameter.push_back(std::make_pair( std::make_pair(L"Roughness : %.2f", &_spheres[0]->material.roughness), L"[3]で増加,[4]で減少"));
	DrawParameterValue.push_back(std::make_pair( std::make_pair(L"LightDirX : %.2f", &_directionalLight->direction.x), L"[←][→]で移動"));
	DrawParameterValue.push_back(std::make_pair( std::make_pair(L"LightDirZ : %.2f", &_directionalLight->direction.z), L"[↑][↓]で移動"));
    return true;
}

void RayTraceScene::Update(const Input& input)
{
	ChangeParameter(input);
#ifdef _DEBUG
	if (!_update)
	{
		if (input.GetKeyboard().IsPush(KEY_INPUT_RETURN))
		{
			_update = true;
		}
		return;
	}
#endif
	DxLib::SetDrawScreen(_screen);
	auto& [screen_width, screen_height] = Application::Instance().GetConfig().GetWindowSize();

	for (int y = 0; y < screen_height; ++y)
	{
		for (int x = 0; x < screen_width; ++x)
		{
			int fixX = x - screen_width / 2 + _eye.x;
			int fixY = screen_height / 2 - y + _eye.y;
			ScreenPos = Position(static_cast<float>(fixX), static_cast<float>(fixY), 0);
			Vector3 vec = (ScreenPos - _eye).ToVector3().GetNormalized();
			Line3D ray(Position(ScreenPos.x, ScreenPos.y, _eye.z), vec);
			Color color = Color(1.0f);
			for (auto& sphere : _spheres)
			{
				color = RecursiveTrace(sphere, ray, y, 0);
			}
			DrawPixel(x, y, color.GetDX_Color()); //球の描画
		}
	}
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	_update = false;
}

void RayTraceScene::Draw(void)
{
	auto& resManager = ResourceManager::Instance();
	int titleFont = resManager.GetDXFont(L"タイトル");
	int explanationFont = resManager.GetDXFont(L"説明");
	DxLib::DrawGraph(0, 0, _screen, false);

	DxLib::DrawBox(0, draw_stateguide_offset_y, 50, fontsize_title + draw_stateguide_offset_y, 0xffffff, true);
	if (stateNum == Phong)
	{
		DxLib::DrawStringToHandle(0, draw_stateguide_offset_y, L"フォン", 0, titleFont);
	}
	else
	{
		DxLib::DrawStringToHandle(0, draw_stateguide_offset_y, L"PBR", 0, titleFont);
	}

	int boxSize = DrawGuidText.size() * fontsize_title * 2 + (DrawExplanationParameter.size() + DrawParameterValue.size()) * (fontsize_title + fontsize_subtitle) + draw_explanation_offset_y;
	DxLib::DrawBox(0, draw_explanation_offset_y, 210, boxSize, 0xffffff, true);

	int x = draw_explanation_offset_x;
	int y = draw_explanation_offset_y;
	for (auto& guid : DrawGuidText)
	{
		DxLib::DrawStringToHandle(x, y, guid.c_str(), 0, explanationFont);
		y += fontsize_title * 2;
	}
	for (auto& param : DrawExplanationParameter)
	{
		DxLib::DrawFormatStringToHandle(x, y, 0, titleFont, param.first.first.c_str(), *param.first.second);
		y += fontsize_title;
		DxLib::DrawFormatStringToHandle(x, y, 0, explanationFont, param.second.c_str());
		y += fontsize_subtitle;
	}
	for (auto& value : DrawParameterValue)
	{
		DxLib::DrawFormatStringToHandle(x, y, 0, titleFont, value.first.first.c_str(), *value.first.second);
		y += fontsize_title;
		DxLib::DrawFormatStringToHandle(x, y, 0, explanationFont, value.second.c_str());
		y += fontsize_subtitle;
	}
}

void RayTraceScene::MoveEye(const MouseInput& input)
{
	if (input.IsHold(EMouseInputType::Right))
	{
		auto vec = input.GetMoveVector();
		_eye.x += static_cast<float>(-vec.x) * cursor_speed;
		_eye.y += static_cast<float>(-vec.y) * cursor_speed;
	}
}

void RayTraceScene::MoveLight(const KeyboardInput& input)
{
	if (input.IsHold(KEY_INPUT_LEFT))
	{
		_directionalLight->direction.x += light_speed;
	}
	else if (input.IsHold(KEY_INPUT_RIGHT))
	{
		_directionalLight->direction.x -= light_speed;
	}
	else if (input.IsHold(KEY_INPUT_UP))
	{
		_directionalLight->direction.z += light_speed;
	}
	else if (input.IsHold(KEY_INPUT_DOWN))
	{
		_directionalLight->direction.z -= light_speed;
	}
	_directionalLight->direction.x = std::clamp(_directionalLight->direction.x, -1.0, 1.0);
	_directionalLight->direction.y = std::clamp(_directionalLight->direction.y, -1.0, 1.0);
	_directionalLight->direction.z = std::clamp(_directionalLight->direction.z, -1.0, 1.0);
}

void RayTraceScene::ModeChange(const KeyboardInput& input)
{
	if (input.IsPush(KEY_INPUT_LCONTROL))
	{
		stateNum = (stateNum + 1) % 2;
		if (stateNum == 0)
		{
			_lightingSphereFunction = &RayTraceScene::Lighting;
			_lightingPlaneFunction = &RayTraceScene::LightingPlane;
		}
		else if (stateNum == 1)
		{
			_lightingSphereFunction = &RayTraceScene::LightingPBR;
			_lightingPlaneFunction = &RayTraceScene::LightingPlanePBR;
		}
	}
}

void RayTraceScene::PBRParameterAdjustment(const KeyboardInput& input)
{
	if (input.IsHold(KEY_INPUT_1))
	{
		for (auto& sphere : _spheres)
		{
			sphere->material.metallic = std::clamp(sphere->material.metallic + 0.01f, 0.0f, 1.0f);
		}
	}
	else if (input.IsHold(KEY_INPUT_2))
	{
		for (auto& sphere : _spheres)
		{
			sphere->material.metallic = std::clamp(sphere->material.metallic - 0.01f, 0.0f, 1.0f);
		}
	}

	if (input.IsHold(KEY_INPUT_3))
	{
		for (auto& sphere : _spheres)
		{
			sphere->material.roughness = std::clamp(sphere->material.roughness + 0.01f, 0.0f, 1.0f);
		}
	}
	else if (input.IsHold(KEY_INPUT_4))
	{
		for (auto& sphere : _spheres)
		{
			sphere->material.roughness = std::clamp(sphere->material.roughness - 0.01f, 0.0f, 1.0f);
		}
	}
}

void RayTraceScene::ChangeParameter(const Input& input)
{
	MoveEye(input.GetMouse());
	auto& key = input.GetKeyboard();
	MoveLight(key);
	ModeChange(key);
	PBRParameterAdjustment(key);
}

Color RayTraceScene::RecursiveTrace(std::shared_ptr<RayTraceSphere>& inSphere, const Line3D& ray, int y, int reflectCount)
{
	Color ret{ 0, 0, 0 };
	ReflectedLightColor reflectColor;
	for (auto& sphere : _spheres)
	{
		auto [isHitPlane, hitPlanePos] = _plane->IsHitRay(ray, _eye);
		bool isHitSphere= sphere->IsHitRay(ray, _eye);
		if (isHitPlane && isHitSphere)
		{
			//床と視点の距離
			auto eyeToPlane = (hitPlanePos - ray.startPosition).ToVector3().Length();
			auto eyeToSphere = (sphere->surfacePos - ray.startPosition).ToVector3().Length();
			if (eyeToPlane < eyeToSphere)
			{
				//床に先に当たった
				(this->*_lightingPlaneFunction)(ray, hitPlanePos, reflectColor);
			}
			else
			{
				//球に先に当たった
				(this->*_lightingSphereFunction)(*sphere, ray, reflectColor);
			}
		}
		else
		{
			//どちらかだけに当たっている
			if (isHitSphere)
			{
				//球にだけ当たっている
				(this->*_lightingSphereFunction)(*sphere, ray, reflectColor);
			}
			else if (isHitPlane)
			{
				//床だけ当たっている
				(this->*_lightingPlaneFunction)(ray, hitPlanePos, reflectColor);
			}
			else
			{
				//空
				float sky = std::clamp(200 + y - 255, 0, 255) / 255.0f;
				reflectColor.directDiffuse.r = sky;
				reflectColor.directDiffuse.g = sky;
				reflectColor.directDiffuse.b = 1.0f;
			}
		}
	}
	ret = reflectColor.directDiffuse + reflectColor.directSpecular + reflectColor.indirectDiffuse + reflectColor.indirectSpecular;
	ret.saturate();
	return ret;
}

void RayTraceScene::LightingPBR(const RayTraceSphere& sphere, const Line3D& eyeRay, ReflectedLightColor& retColor)
{
	auto incidentLight = IncidentLight(_directionalLight->direction, _directionalLight->lightColor);
	RenderEquations(incidentLight, sphere.normal, eyeRay.vector, sphere.material, retColor);
	auto rvec = eyeRay.vector.Reflect(sphere.normal).GetNormalized();
	auto [hit, hitPos] = _plane->IsHitRay(Line3D(sphere.surfacePos, rvec), _eye);
	if (hit)
	{
		retColor.directDiffuse *= Lerp((_plane->GetCheckerColorFromPosition(hitPos)), Color(1.0f), 1.0 - sphere.material.metallic);
		retColor.directSpecular *= Lerp((_plane->GetCheckerColorFromPosition(hitPos)), Color(1.0f), 1.0 - sphere.material.metallic);
	}
	else
	{
		float val = 0.0f;
		val = std::clamp((1.0 - rvec.y), 0.0, 1.0);
		auto sky = Color(val * sphere.material.metallic, val * sphere.material.metallic, 1.0f * sphere.material.metallic);
		retColor.directDiffuse *= Lerp(sky, Color(1.0f), 1.0 - sphere.material.metallic);
	}
}

void RayTraceScene::LightingPlanePBR(const Line3D& eyeRay, const Position& hitPos, ReflectedLightColor& retColor)
{
	auto incidentLight = IncidentLight(_directionalLight->direction, _directionalLight->lightColor);
	RenderEquations(incidentLight, _plane->plane.normal, eyeRay.vector, _plane->material, retColor);
	retColor.directDiffuse *= _plane->GetCheckerColorFromPosition(hitPos);
	bool isShielding = false;
	isShielding = IsReachingLight(*_directionalLight, hitPos);
	if (isShielding)
	{
		retColor.directDiffuse *= {0.1f, 0.1f, 0.1f};
		retColor.directSpecular *= {0.1f, 0.1f, 0.1f};
	}
}


void RayTraceScene::LightingPlane(const Line3D& eyeRay, const Position& hitPos, ReflectedLightColor& retColor)
{
	retColor.directDiffuse = _plane->GetCheckerColorFromPosition(hitPos);
	retColor.directSpecular = GetPlaneSpecularColor(eyeRay);
	bool isShielding = false;
	isShielding = IsReachingLight(*_directionalLight, hitPos);
	if (isShielding)
	{
		retColor.directDiffuse *= {0.1f, 0.1f, 0.1f};
		retColor.directSpecular *= {0.1f, 0.1f, 0.1f};
	}
}

Color RayTraceScene::GetPlaneSpecularColor(const Line3D& eyeRay)
{
	float val = 0.0f;
	auto rvec = eyeRay.vector.Reflect(_plane->plane.normal);
	val += Dot(rvec, -_directionalLight->direction.GetNormalized());
	val = std::clamp(val, 0.0f, 1.0f);
	val = std::powf(val, 20.0f);
	return Color(val, val, val);;
}

void RayTraceScene::Lighting(const RayTraceSphere& sphere, const Line3D& eyeRay, ReflectedLightColor& retColor)
{
	retColor.directDiffuse = GetSphereSurfaceColor(sphere, eyeRay);
	retColor.directSpecular = GetSphereSpecularColor(sphere, eyeRay);
}

Color RayTraceScene::GetSphereSurfaceColor (const RayTraceSphere& sphere, const Line3D& eyeRay)
{
	Color ret;
	float val = 0.0f;
	val += Dot(sphere.normal, -_directionalLight->direction);
	val = std::clamp(val, 0.0f, 1.0f);

	ret = sphere.material.albedo * val;

	auto rvec = eyeRay.vector.Reflect(sphere.normal).GetNormalized();
	auto [hit, hitPos] = _plane->IsHitRay(Line3D(sphere.surfacePos, rvec), _eye);
	if (hit)
	{
		ret *= Lerp((_plane->GetCheckerColorFromPosition(hitPos)), Color(1.0f), 1.0 - sphere.material.metallic);
	}
	else
	{
		val = std::clamp((1.0 - rvec.y), 0.0, 1.0);
		auto sky = Color(val * sphere.material.metallic, val * sphere.material.metallic, 1.0f * sphere.material.metallic);
		ret *= Lerp(sky, Color(1.0f), 1.0 - sphere.material.metallic);
	}
	return ret;
}

Color RayTraceScene::GetSphereSpecularColor(const RayTraceSphere& sphere, const Line3D& eyeRay)
{
	float val = 0.0f;
	auto rvec = eyeRay.vector.Reflect(sphere.normal);
	val += Dot(rvec, -_directionalLight->direction.GetNormalized());
	val = std::clamp(val, 0.0f, 1.0f);
	val = std::powf(val, 20.0f);
	return Color(val, val, val);;
}

bool RayTraceScene::IsReachingLight(const DirectionalLight& light, const Position& pos)
{
	bool ret = true;
	for (auto& sphere : _spheres)
	{
		ret &= sphere->IsHitRay(Line3D(pos, -light.direction), pos);
	}
	return ret;
}