#include "SceneBase.h"

SceneBase::SceneBase(SceneController& c, std::wstring sceneName):_sceneController(c)
{
	_sceneName = sceneName;
	_frameCount = 0;
}
