#include <DxLib.h>
#include "SceneController.h"
#include "RayTraceScene.h"
#include "ResourceManager.h"

void SceneController::AddPostDrawUpdate(std::function<void(void)> func)
{
	_postDrawUpdater.push_back(func);
}

SceneController::SceneController()
{
	_scene.emplace_back(new RayTraceScene(*this));
	auto& resManager = ResourceManager::Instance();
	resManager.CreateDXFont(L"Title", L"タイトル", fontsize_title, 4, DX_FONTTYPE_NORMAL);
	resManager.CreateDXFont(L"SubTitle", L"説明", fontsize_subtitle, 2, DX_FONTTYPE_NORMAL);
}

void SceneController::ChangeScene(SceneBase* scene)
{
	auto change = [this, scene]()
	{
		_scene.clear();
		_scene.emplace_back(scene);
	};
	_postDrawUpdater.push_back(change);
}

void SceneController::Update(const Input& input)
{
	if (_scene.empty())
	{
		return;
	}
	_scene.back()->Update(input);
}

void SceneController::Draw()
{
	if (_scene.empty())
	{
		return;
	}

	auto rit = _scene.begin();
	for (; rit != _scene.end(); rit++)
	{
		(*rit)->Draw();
		DxLib::DrawBox(0, 30, 200, 46, 0xffffff, true);
		DxLib::DrawString(30, 30, (*rit)->_sceneName.c_str(), 0);
	}
}

void SceneController::PostDrawUpdate(void)
{
	if (_postDrawUpdater.empty())
	{
		return;
	}

	for (auto& func : _postDrawUpdater)
	{
		func();
	}
	_postDrawUpdater.clear();
}
