#pragma once
#include "SceneBase.h"
#include "RayTraceScene.h"

class AppMainScene : public SceneBase
{
	friend SceneController;
	friend RayTraceScene;
private:
	AppMainScene(SceneController& c);
	virtual	bool Initialize(void) override final;
	virtual void Update(const Input& input) override final;
	virtual void Draw(void) override final;
	/// <summary>
	/// 毎フレームの更新時に呼び出す関数ポインタ
	/// </summary>
	void (AppMainScene::* _updater)(const Input&) = nullptr;

	/// <summary>
	/// 毎フレームの描画時に呼び出す関数ポインタ
	/// </summary>
	void (AppMainScene::* _drawer)(void) = nullptr;
};