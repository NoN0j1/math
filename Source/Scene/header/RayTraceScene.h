#pragma once
#include "SceneBase.h"
#include "Light.h"

class RayTraceScene : public SceneBase
{
	friend SceneController;
private:
	RayTraceScene(SceneController& c);
	virtual bool Initialize(void) override;
	virtual void Update(const Input& input) override;
	virtual void Draw(void) override;

	/// <summary>
	/// 視点を動かす
	/// </summary>
	/// <param name="input"></param>
	void MoveEye(const MouseInput& input);

	/// <summary>
	/// ライトを動かす
	/// </summary>
	/// <param name="input">キーボード入力情報</param>
	void MoveLight(const KeyboardInput& input);

	/// <summary>
	/// ライティング切替
	/// </summary>
	/// <param name="input">キーボード入力情報</param>
	void ModeChange(const KeyboardInput& input);

	/// <summary>
	/// PBRの際のパラメータを変更する
	/// </summary>
	/// <param name="input">キーボード入力情報</param>
	void PBRParameterAdjustment(const KeyboardInput& input);

	/// <summary>
	/// 各種パラメータの調整を行う
	/// </summary>
	/// <param name="input">入力情報</param>
	void ChangeParameter(const Input& input);

	/// <summary>
	/// PBRで球のライティングを行う
	/// </summary>
	/// <param name="sphere">ライティング対象</param>
	/// <param name="ray">視線ベクトル</param>
	/// <param name="retColor">計算結果</param>
	void LightingPBR(const RayTraceSphere& sphere, const Line3D& ray, ReflectedLightColor& retColor);

	/// <summary>
	/// PBRで床のライティングを行う
	/// </summary>
	/// <param name="eyeRay">視線ベクトル</param>
	/// <param name="hitPos">視線が当たった床の座標</param>
	/// <param name="retColor">計算結果</param>
	void LightingPlanePBR(const Line3D& eyeRay, const Position& hitPos, ReflectedLightColor& retColor);

	/// <summary>
	/// フォンで床のライティングを行う
	/// </summary>
	/// <param name="eyeRay">視線ベクトル</param>
	/// <param name="hitPos">視線が当たった床の座標</param>
	/// <param name="retColor">計算結果</param>
	void LightingPlane(const Line3D& eyeRay, const Position& hitPos, ReflectedLightColor& retColor);

	/// <summary>
	/// 球の表面の反射色を計算する
	/// </summary>
	/// <param name="eyeRay">視線ベクトル</param>
	/// <returns>描画する色</returns>
	Color GetPlaneSpecularColor(const Line3D& eyeRay);

	/// <summary>
	/// フォンライティングを行う
	/// </summary>
	/// <param name="sphere">ライティング対象</param>
	/// <param name="ray">視線ベクトル</param>
	/// <param name="retColor">k計算結果</param>
	void Lighting(const RayTraceSphere& sphere, const Line3D& ray, ReflectedLightColor& retColor);

	/// <summary>
	/// 球の表面の描画色を計算する
	/// </summary>
	/// <param name="sphere">対象の球</param>
	/// <param name="eyeRay">視線ベクトル</param>
	/// <returns>描画する色</returns>
	Color GetSphereSurfaceColor(const RayTraceSphere& sphere, const Line3D& eyeRay);

	/// <summary>
	/// 球の表面の反射色を計算する
	/// </summary>
	/// <param name="sphere">対象の球</param>
	/// <param name="eyeRay">視線ベクトル</param>
	/// <returns>描画する色</returns>
	Color GetSphereSpecularColor(const RayTraceSphere& sphere, const Line3D& eyeRay);

	/// <summary>
	/// 球のライティングStateポインタ
	/// </summary>
	void (RayTraceScene::*_lightingSphereFunction)(const RayTraceSphere& sphere, const Line3D& eyeRay, ReflectedLightColor& retColor);

	/// <summary>
	/// 床のライティングStateポインタ
	/// </summary>
	void (RayTraceScene::* _lightingPlaneFunction)(const Line3D& eyeRay, const Position& hitPos, ReflectedLightColor& retColor);

	/// <summary>
	/// ライトがその座標に届いているかを取得する
	/// </summary>
	/// <param name="light">DirectionalLight</param>
	/// <param name="pos">確認する座標</param>
	/// <returns>true = 届いている, false = 届いていない</returns>
	bool IsReachingLight(const DirectionalLight& light, const Position& pos);

	/// <summary>
	/// 各ピクセル単位でトレースを行ない、描画色を取得する
	/// </summary>
	/// <param name="ray">目線</param>
	/// <param name="y">スクリーン上の高さ</param>
	/// <returns>描画色</returns>
	Color RecursiveTrace(std::shared_ptr<RayTraceSphere>& inSphere, const Line3D& ray, int y, int reflectCount);

	/// <summary>
	/// 視点座標
	/// </summary>
	Position _eye;

	/// <summary>
	/// 床
	/// </summary>
	std::shared_ptr<RayTracePlane> _plane;

	/// <summary>
	/// 配置されている球リスト
	/// </summary>
	std::vector<std::shared_ptr<RayTraceSphere>> _spheres;

	/// <summary>
	/// 配置されている平行光源
	/// </summary>
	std::shared_ptr<DirectionalLight> _directionalLight;

	/// <summary>
	/// 配置されているスポットライト
	/// </summary>
	std::shared_ptr<SpotLight> _spotLight;

	/// <summary>
	/// 配置されているポイントライト
	/// </summary>
	std::shared_ptr<PointLight> _pointLight;

	/// <summary>
	/// 描画を更新するかどうかのフラグ(重いので)
	/// </summary>
	bool _update;

	/// <summary>
	/// 描画先
	/// </summary>
	int _screen;
};

