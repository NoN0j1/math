#pragma once
#include "SceneController.h"
#include "Input.h"

class SceneBase
{
	friend SceneController;
public:
	virtual ~SceneBase() = default;

	/// <summary>
	/// 初期化
	/// </summary>
	/// <returns>true = 成功, false = 失敗</returns>
	virtual bool Initialize(void) = 0;

	/// <summary>
	/// 毎フレームの更新
	/// </summary>
	/// <param name = "input">キー等の入力情報</param>
	virtual void Update(const Input& input) = 0;

	/// <summary>
	/// 毎フレームの描画
	/// </summary>
	virtual void Draw(void) = 0;

protected:
	SceneBase() = delete;
	SceneBase(SceneController& c, std::wstring sceneName);

	/// <summary>
	/// SceneControllerの実体参照
	/// </summary>
	SceneController& _sceneController;

	/// <summary>
	/// シーンが開始してからのフレーム数
	/// </summary>
	unsigned long long _frameCount;

	/// <summary>
	/// シーン名
	/// </summary>
	std::wstring _sceneName;
};