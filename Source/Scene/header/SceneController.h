#pragma once
#include <memory>
#include <deque>
#include <functional>
#include "Application.h"
#include "SceneBase.h"

constexpr int fontsize_title = 16;
constexpr int fontsize_subtitle = 12;

class SceneBase;

/// <summary>
/// シーン遷移を制御するクラス
/// </summary>
class SceneController
{
	friend class Application;
public:
	~SceneController() = default;

	/// <summary>
	/// シーンを切り替える
	/// </summary>
	/// <param name="scene">切り替えるシーン</param>
	void ChangeScene(SceneBase* scene);

	/// <summary>
	/// シーンの描画後に行なう処理を登録する
	/// </summary>
	/// <param name="func"></param>
	void AddPostDrawUpdate(std::function<void(void)> func);

private:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	SceneController();

	/// <summary>
	/// 現在のスタックで一番上にあるシーンのUpdateを呼び出す
	/// </summary>
	/// <param name = "input">各シーンに受け流す入力情報</param>
	void Update(const Input& input);

	/// <summary>
	/// 現在のスタックで一番上にあるシーンのDrawを呼び出す
	/// </summary>
	void Draw(void);

	/// <summary>
	/// 描画後更新処理
	/// </summary>
	/// <param name=""></param>
	void PostDrawUpdate(void);

	/// <summary>
	/// 管理しているシーン
	/// </summary>
	std::deque<std::shared_ptr<SceneBase>> _scene;

	/// <summary>
	/// 描画後アップデート項目
	/// </summary>
	std::vector<std::function<void()>> _postDrawUpdater;
};